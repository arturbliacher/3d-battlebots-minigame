/*******************************************************************
		   Hierarchical Multi-Part Model Example
********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
#include <utility>
#include <vector>
#include "VECTOR3D.h"
#include "cube.h"
#include "QuadMesh.h"

#include <iostream>
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <windows.h>
#include <gl/glu.h>
#include <gl/gl.h>
#include <gl/glut.h>
#endif

const int vWidth  = 1000;    // Viewport width in pixels
const int vHeight = 700;    // Viewport height in pixels


// Camera Coordiantes
float xCam = 10.0;
float yCam = 16.0;
float zCam = 20.0;

// Note how everything depends on robot body dimensions so that can scale entire robot proportionately
// just by changing robot body scale

float robotBodyWidth = 8.0;
float robotBodyLength = 2.0;
float robotBodyDepth = 6.0;

float botX = 0.0;
float botY = 0.0;
float botZ = 0.0;

float robotAngle = 0.0;

float wheelDiameter = 0.6*robotBodyLength;
float wheelRotation = 0.0;

float stanchionLength = 5;
float stanchionRadius = 0.2;
float stanchionAngle = 15.0f;

float sawRotation = 0;

// Spin Cube Mesh
float cubeAngle = 0.0;
float cubeX = 0.0;
float cubeY = 10.0;
float cubeZ = 0.0;

// Lighting/shading and material properties for robot - upcoming lecture - just copy for now
// Robot RGBA material properties (NOTE: we will learn about this later in the semester)
GLfloat robotBody_mat_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat robotBody_mat_specular[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat robotBody_mat_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat robotBody_mat_shininess[] = { 20.0F };

GLfloat robotLowerBody_mat_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat robotLowerBody_mat_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat robotLowerBody_mat_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat robotLowerBody_mat_shininess[] = { 76.8F };


// Light properties
GLfloat light_position0[] = { -4.0F, 8.0F, 8.0F, 1.0F };
GLfloat light_position1[] = { 4.0F, 8.0F, 8.0F, 1.0F };
GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat light_ambient[] = { 0.2F, 0.2F, 0.2F, 1.0F };


// Mouse button
int currentButton;

// A template cube mesh
CubeMesh *cubeMesh = createCubeMesh();

// A flat open mesh
QuadMesh *groundMesh = NULL;

// Structure defining a bounding box, currently unused
typedef struct BoundingBox {
    VECTOR3D min;
	VECTOR3D max;
} BBox;

// Default Mesh Size
int meshSize = 64;

// Prototypes for functions in this module
void initOpenGL();
void display(void);
void reshape(int w, int h);
void mouse(int button, int state, int x, int y);
void mouseMotionHandler(int xMouse, int yMouse);
void keyboard(unsigned char key, int x, int y);
void keyRelease(unsigned char key, int x, int y);
void functionKeys(int key, int x, int y);
void animationHandler(int param);
void drawRobot();
void drawBody();
void drawWheels();
void drawSawArm();
void drawSaw();
void rotateBot(unsigned char key);

int main(int argc, char **argv)
{
	// Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(vWidth, vHeight);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("3D Hierarchical Example");

	// Initialize GL
	initOpenGL();
    animationHandler(10);

	// Register callback functions
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
	glutMotionFunc(mouseMotionHandler);
	glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyRelease);
	glutSpecialFunc(functionKeys);

	// Start event loop, never returns
	glutMainLoop();

	return 0;
}


// Set up OpenGL. For viewport and projection setup see reshape(). 
void initOpenGL()
{
	// Set up and enable lighting
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);

	glLightfv(GL_LIGHT0, GL_POSITION, light_position0);
	glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);   // This second light is currently off

	// Other OpenGL setup
	glEnable(GL_DEPTH_TEST);   // Remove hidded surfaces
	glShadeModel(GL_SMOOTH);   // Use smooth shading, makes boundaries between polygons harder to see 
	glClearColor(0.4F, 0.4F, 0.4F, 0.0F);  // Color and depth for glClear
	glClearDepth(1.0f);
	glEnable(GL_NORMALIZE);    // Renormalize normal vectors 
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   // Nicer perspective

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	// Other initializatuion
	// Set up ground quad mesh
    VECTOR3D origin = VECTOR3D(-64, 0.0, 64);
	VECTOR3D dir1v = VECTOR3D(1.0f, 0.0f, 0.0f);
	VECTOR3D dir2v = VECTOR3D(0.0f, 0.0f, -1.0f);
	groundMesh = new QuadMesh(meshSize, 64.0);
    groundMesh->InitMesh(meshSize, origin, 128, 128, dir1v, dir2v);

	VECTOR3D ambient = VECTOR3D(0.0f, 0.05f, 0.0f);
	VECTOR3D diffuse = VECTOR3D(0.4f, 0.8f, 0.4f);
	VECTOR3D specular = VECTOR3D(0.04f, 0.04f, 0.04f);
	float shininess = 0.2;
	groundMesh->SetMaterial(ambient, diffuse, specular, shininess);

}


// Callback, called whenever GLUT determines that the window should be redisplayed
// or glutPostRedisplay() has been called.
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	// Create Viewing Matrix V
    
	// Set up the camera at position (0, 6, 22) looking at the origin, up along positive y axis
    gluLookAt(xCam, yCam, zCam, 0.0, 0.0, -0.0, 0.0, 1.0, 0.0);

	// Draw Robot

	// Apply modelling transformations M to move robot
	// Current transformation matrix is set to IV, where I is identity matrix
	// CTM = IV
    
	drawRobot();
    
	// Example of drawing a mesh (closed cube mesh)
	glPushMatrix();
    
    glRotatef(cubeAngle, 0.0, 1.0, 0.0);
	glTranslatef(cubeX, cubeY, cubeZ);
	drawCubeMesh(cubeMesh);
	glPopMatrix();
     
	// Draw ground
	glPushMatrix();
    glTranslatef(0.0, -0.8, 0.0);
	groundMesh->DrawMesh(meshSize);
	glPopMatrix();

	glutSwapBuffers();   // Double buffering, swap buffers
}

void drawRobot()
{
	glPushMatrix();

    drawBody();
    drawWheels();
    drawSawArm();
    drawSaw();
	glPopMatrix();
}


void drawBody()
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, robotBody_mat_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, robotBody_mat_specular);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, robotBody_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, robotBody_mat_shininess);

	glPushMatrix();
    glTranslatef(botX, botY, botZ);
    glRotatef(robotAngle, 0.0f, 1.0f, 0.0f);
	glScalef(robotBodyWidth, robotBodyLength, robotBodyDepth);
	glutSolidCube(0.5);
	glPopMatrix();
}

void drawWheels()
{
	// Set robot material properties per body part. Can have seperate material properties for each part
	glMaterialfv(GL_FRONT, GL_AMBIENT, robotBody_mat_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, robotBody_mat_specular);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, robotBody_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, robotBody_mat_shininess);

    ///////////////////////////////////////////////////////////////////Front Right////////////////////////////////////////////////////////////////////////////
	glPushMatrix();
	// Position wheel with respect to parent (body)
    glTranslatef(botX, botY, botZ);
    glRotatef(robotAngle, 0.0f, 1.0f, 0.0f);
    glTranslatef(robotBodyLength*0.7, -0.2, 1.5);
	// Build wheel
	glPushMatrix();
	GLUquadricObj *quadraticFR;
    quadraticFR = gluNewQuadric();
    glRotatef(0.0f, wheelRotation, 0.0f, 0.0f);
    gluCylinder(quadraticFR,wheelDiameter/2,wheelDiameter/2,0.3,32,32);
    
    //Cap wheel on both sides
    glPushMatrix();
    glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
    gluDisk(quadraticFR, 0.0f, wheelDiameter/2, 30, 1);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, 0.3);
    gluDisk(quadraticFR, 0.0f, wheelDiameter/2, 30, 1);
    glPopMatrix();
    
    gluDeleteQuadric(quadraticFR);
    glPopMatrix();
    glPopMatrix();
    
    
    //////////////////////////////////////////////////////////////////////Front Left////////////////////////////////////////////////////////////////////////////////////
    glPushMatrix();
    // Position wheel with respect to parent (body)
    glTranslatef(botX, botY, botZ);
    glRotatef(robotAngle, 0.0f, 1.0f, 0.0f);
    glTranslatef(robotBodyLength*0.7, -0.2, -1.8);
    // Build wheel
    glPushMatrix();
    GLUquadricObj *quadraticFL;
    quadraticFL = gluNewQuadric();
    glRotatef(0.0f, wheelRotation, 0.0f, 0.0f);
    gluCylinder(quadraticFL,wheelDiameter/2,wheelDiameter/2,0.3,32,32);
    
    //Cap wheel on both sides
    glPushMatrix();
    glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
    gluDisk(quadraticFL, 0.0f, wheelDiameter/2, 30, 1);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, 0.3);
    gluDisk(quadraticFL, 0.0f, wheelDiameter/2, 30, 1);
    glPopMatrix();
    
    gluDeleteQuadric(quadraticFL);
    glPopMatrix();
    glPopMatrix();

    glPopMatrix();
    
    //////////////////////////////////////////////////////////////////////Back Left////////////////////////////////////////////////////////////////////////////////////
    glPushMatrix();
    // Position wheel with respect to parent (body)
    glTranslatef(botX, botY, botZ);
    glRotatef(robotAngle, 0.0f, 1.0f, 0.0f);
    glTranslatef(-robotBodyLength*0.7, -0.2, -1.8);
    // Build wheel
    glPushMatrix();
    GLUquadricObj *quadraticBL;
    quadraticBL = gluNewQuadric();
    glRotatef(0.0f, wheelRotation, 0.0f, 0.0f);
    gluCylinder(quadraticBL,wheelDiameter/2,wheelDiameter/2,0.3,32,32);
    
    //Cap wheel on both sides
    glPushMatrix();
    glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
    gluDisk(quadraticBL, 0.0f, wheelDiameter/2, 30, 1);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, 0.3);
    gluDisk(quadraticBL, 0.0f, wheelDiameter/2, 30, 1);
    glPopMatrix();
    
    gluDeleteQuadric(quadraticBL);
    glPopMatrix();
    glPopMatrix();

    glPopMatrix();
    
    //////////////////////////////////////////////////////////////////////Back Right////////////////////////////////////////////////////////////////////////////////////
    glPushMatrix();
    // Position wheel with respect to parent (body)
    glTranslatef(botX, botY, botZ);
    glRotatef(robotAngle, 0.0f, 1.0f, 0.0f);
    glTranslatef(-robotBodyLength*0.7, -0.2, 1.5);
    // Build wheel
    glPushMatrix();
    GLUquadricObj *quadraticBR;
    quadraticBR = gluNewQuadric();
    glRotatef(0.0f, wheelRotation, 0.0f, 0.0f);
    gluCylinder(quadraticBR,wheelDiameter/2,wheelDiameter/2,0.3,32,32);
    
    //Cap wheel on both sides
    glPushMatrix();
    glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
    gluDisk(quadraticBR, 0.0f, wheelDiameter/2, 30, 1);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, 0.3);
    gluDisk(quadraticBR, 0.0f, wheelDiameter/2, 30, 1);
    glPopMatrix();
    
    gluDeleteQuadric(quadraticBR);
    glPopMatrix();

    glPopMatrix();
}

void drawSawArm()
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, robotLowerBody_mat_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, robotLowerBody_mat_specular);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, robotLowerBody_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, robotLowerBody_mat_shininess);

	glPushMatrix();

	// stanchion
	glPushMatrix();
    glTranslatef(botX, botY, botZ);
    glRotatef(robotAngle, 0.0f, 1.0f, 0.0f);
    glRotatef(stanchionAngle, 0.0f, 0.0f, 1.0f);
    glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	gluCylinder(gluNewQuadric(), stanchionRadius, stanchionRadius, stanchionLength, 20, 10);
	glPopMatrix();

	glPopMatrix();
}

void drawSaw(){
    glPushMatrix();
    glTranslatef(botX, botY, botZ);
    glRotatef(robotAngle, 0.0f, 1.0f, 0.0f);
    glTranslatef(4.5, 1.25, 0);
    glRotatef(sawRotation, 0.0f, 0.0f, -1.0f);
    glScalef(2, 2, 0.1);
    glutSolidOctahedron();
    glPopMatrix();
}

// Callback, called at initialization and whenever user resizes the window.
void reshape(int w, int h)
{
	// Set up viewport, projection, then change to modelview matrix mode - 
	// display function will then set up camera and do modeling transforms.
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    gluPerspective(60, (GLdouble)w / h, 0.6, 300.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Set up the camera at position (0, 6, 22) looking at the origin, up along positive y axis
	gluLookAt(xCam, yCam, zCam, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

bool stop = true;
bool forward = false;
bool backward = false;
bool left = false;
bool right = false;

void keyRelease(unsigned char key, int x, int y) {
    switch (key) {
        case 'w':
            forward = false;
            break;
        case 's':
            backward = false;
            break;
        case 'a':
            left = false;
            break;
        case 'd':
            right = false;
            break;
            
        default:
            break;
    }
}
// Callback, handles input from the keyboard, non-arrow keys
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
        case ' ':
            if (stop == false){
                stop = true;
            }else{
                stop = false;
            }
            break;
        case 'i':
            yCam += 1;
            break;
        case 'k':
            yCam -= 1;
            break;
        case 'j':
            xCam -= 1;
            break;
        case 'l':
            xCam += 1;
            break;
        case 'p':
            zCam -= 1;
            break;
        case ';':
            zCam += 1;
            break;
        case 'w':
            forward = true;
            break;
        case 's':
            backward = true;
            break;
        case 'a':
            left = true;
            break;
        case 'd':
            right = true;
            break;
	}

	glutPostRedisplay();   // Trigger a window redisplay
}

float speed = 0.6;

void animationHandler(int param)
{
	if (!stop){
        sawRotation += 40;
	}
    if (forward){
        wheelRotation += 1;
        botX += speed*((float)cos(robotAngle * M_PI / 180));
        botZ -= speed*((float)sin(robotAngle * M_PI / 180));
    }
    if (backward){
        wheelRotation -= 1;
        botX -= speed*((float)cos(robotAngle * M_PI / 180));
        botZ += speed*((float)sin(robotAngle * M_PI / 180));
    }
    if (left){
        robotAngle += 3;
    }
    if (right){
        robotAngle -= 3;
    }
    glutTimerFunc(10, animationHandler, 0);
    glutPostRedisplay();
}



// Callback, handles input from the keyboard, function and arrow keys
void functionKeys(int key, int x, int y)
{
	// Help key
	if (key == GLUT_KEY_F1)
	{
        printf("\'w\' Move Bot Forward\n\'s\' Move Bot Backwards\n\'a\' Turn Left\n\'d\' Turn Right\n\'space\' Toggle Saw\n\'i\' Moves Camera +Y\n\'k\' Moves Camera -Y\n\'j\' Moves Camera -X\n\'l\' Moves Camera +X\n\'p\' Moves Camera +Z\n\';\' Moves Camera -Z\n");
	}

	glutPostRedisplay();   // Trigger a window redisplay
}


// Mouse button callback - use only if you want to 
void mouse(int button, int state, int x, int y)
{
	currentButton = button;

	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN)
		{
			;

		}
		break;
	case GLUT_RIGHT_BUTTON:
		if (state == GLUT_DOWN)
		{
			;
		}
		break;
	default:
		break;
	}

	glutPostRedisplay();   // Trigger a window redisplay
}


// Mouse motion callback - use only if you want to 
void mouseMotionHandler(int xMouse, int yMouse)
{
	if (currentButton == GLUT_LEFT_BUTTON)
	{
		;
	}

	glutPostRedisplay();   // Trigger a window redisplay
}

